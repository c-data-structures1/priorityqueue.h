/**
 * Copyright (C) 2020 Fabio Sussarellu
 *
 * This file is part of CDataStructures.
 *
 * CDataStructures is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CDataStructures is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CDataStructures.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PQITEM_H
#define PQITEM_H

// * DNT
#include "../Heap.h/Array.h/CUtils.h/CUtils.h"
typedef struct PQItem {
    Item I;
    int Priority;
} * PQItem;
// * EDNT

void PQItem__free(PQItem i);
PQItem PQItem__new(Item I, int Priority);

#endif // ! PQITEM_H
